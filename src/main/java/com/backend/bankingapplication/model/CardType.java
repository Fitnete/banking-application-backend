package com.backend.bankingapplication.model;

public enum CardType {
    DEBIT, CREDIT
}
