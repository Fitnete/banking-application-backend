package com.backend.bankingapplication.controller;

import com.backend.bankingapplication.model.CardModel;
import com.backend.bankingapplication.model.CardType;
import com.backend.bankingapplication.service.CardService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController()
public class CardController {

    private static final Logger logger = LoggerFactory.getLogger(CardController.class);
    @Autowired
    private CardService cardService;

    @PostMapping("/new-debit-card")
    @PreAuthorize("hasRole('client')")
    public CardModel createDebitCard(@RequestBody CardModel cardModel) {
        return cardService.createDebitCard(cardModel);
    }

    @PostMapping("/new-credit-card")
    @PreAuthorize("hasRole('client')")
    public CardModel createCreditCardRequest(@RequestBody CardModel cardModel) {
        return cardService.createCreditCardRequest(cardModel);
    }

    @PostMapping("/approve-card/")
    @PreAuthorize("hasRole('teller')")
    public CardModel approveCard(@RequestBody CardModel cardModel) throws Exception {
        logger.info("A card is approved");
        return cardService.approveCard(cardModel);
    }

    @GetMapping("/cardTypes")
    public CardType[] getCardTypes() {
        return cardService.getCardTypes();
    }

    @GetMapping("/cards")
    @PreAuthorize("hasRole('teller')")
    public List<CardModel> getAllCards() {
        return cardService.getAllCards();
    }

    @GetMapping("/card/{id}")
    @PreAuthorize("hasRole('teller')")
    public Optional<CardModel> getClientCard(@PathVariable("id") Long id) {
        return cardService.getClientCard(id);
    }

    @GetMapping("/cards/{user_id}")
    @PreAuthorize("hasRole('client')")
    public List<CardModel> getClientCards(@PathVariable String user_id) {
        return cardService.getClientCards(user_id);
    }
}
