package com.backend.bankingapplication.service;


import com.backend.bankingapplication.model.RoleModel;
import com.backend.bankingapplication.model.UserModel;
import com.backend.bankingapplication.repository.RoleRepository;
import com.backend.bankingapplication.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;


@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    public String getEncodedPassword(String password) {
        return passwordEncoder.encode(password);
    }

    public List<UserModel> getTellers() {
        return userRepository.findUserModelsByRoles(roleRepository.findById("teller"));
    }

    public List<UserModel> getClients() {
        return userRepository.findUserModelsByRoles(roleRepository.findById("client"));
    }

    public UserModel createTeller(UserModel teller) {
        teller.setUserPassword(getEncodedPassword(teller.getUserPassword()));
        return userRepository.save(teller);
    }

    public UserModel createClient(UserModel client) {
        client.setUserPassword(getEncodedPassword(client.getUserPassword()));
        return userRepository.save(client);
    }

    public List<RoleModel> getRoles() {
        return (List<RoleModel>) roleRepository.findAll();
    }

    public UserModel getUser(String userName) throws Exception {
        return userRepository.findById(userName).orElseThrow(() -> new Exception("Can not find this user"));
    }

    public void deleteUser(String userName) {
        userRepository.deleteById(userName);
    }

    public void initRoleAndUser() {

//        RoleModel adminRole = new RoleModel();
//        adminRole.setRoleName("admin");
//        adminRole.setRoleDesc("Administrator of the application");
//        roleRepository.save(adminRole);

//        UserModel adminUser = new UserModel();
//        adminUser.setUserName("neta");
//        adminUser.setUserPassword(getEncodedPassword("123456"));
//        adminUser.setFirstName("Neta");
//        adminUser.setLastName("Shehu");
//        adminUser.setEmail("neta.23041990@gmail.com");
//        Set<RoleModel> adminRoles = new HashSet<>();
//        adminRoles.add(adminRole);
//        adminUser.setRoles(adminRoles);
//        userRepository.save(adminUser);

    }


}

