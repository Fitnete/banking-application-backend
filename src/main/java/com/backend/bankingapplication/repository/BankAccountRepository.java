package com.backend.bankingapplication.repository;

import com.backend.bankingapplication.model.BankAccountModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;


public interface BankAccountRepository extends CrudRepository<BankAccountModel, Long> {

    @Query("select b from BankAccountModel b where b.user_id = ?1")
    List<BankAccountModel> findByUser_id(String user_id);

}
