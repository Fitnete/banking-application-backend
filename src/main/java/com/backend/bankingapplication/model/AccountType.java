package com.backend.bankingapplication.model;

public enum AccountType {
    CURRENT, TECHNICAL
}

