package com.backend.bankingapplication.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class JwtResponse {
    private UserModel user;
    private String jwtToken;

}

