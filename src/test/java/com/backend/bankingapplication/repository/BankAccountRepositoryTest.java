package com.backend.bankingapplication.repository;

import com.backend.bankingapplication.model.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.jdbc.DataJdbcTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

@DataJpaTest
class BankAccountRepositoryTest {

    @Autowired
    private BankAccountRepository underTest;
    String user_id = "client3";

    @Test
    void findByUser_id() {
        BankAccountModel bankAccount = new BankAccountModel(
                1L,
                "AL ",
                Currency.EURO,
                0.00,
                AccountType.CURRENT,
                0.00,
                false,
                "client3",
                new CardModel(),
                new ArrayList<TransactionModel>()
        );
        underTest.save(bankAccount);

        List<BankAccountModel> bankAccounts = underTest.findByUser_id(user_id);

        assertThat(bankAccounts.size() > 0);

    }
}
