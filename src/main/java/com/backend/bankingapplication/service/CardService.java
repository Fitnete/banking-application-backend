package com.backend.bankingapplication.service;

import com.backend.bankingapplication.model.*;
import com.backend.bankingapplication.repository.BankAccountRepository;
import com.backend.bankingapplication.repository.CardRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.Optional;


@Service
public class CardService {

    @Autowired
    private CardRepository cardRepository;

    @Autowired
    private BankAccountService bankAccountService;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    public CardModel createDebitCard(CardModel cardModel) {
        return cardRepository.save(cardModel);
    }

    public CardModel createCreditCardRequest(CardModel cardModel) {
        return cardRepository.save(cardModel);
    }


    public CardType[] getCardTypes() {
        return CardType.values();
    }

    @Transactional
    public CardModel approveCard(CardModel cardModel) throws Exception {
        String cardModelType = cardModel.getCardType().toString();
        if (Objects.equals(cardModelType, "CREDIT")) {
            BankAccountModel technicalBankAccount = this.bankAccountService.createBankAccount(new BankAccountModel());
            technicalBankAccount.setApproved(true);
            technicalBankAccount.setCurrency(Currency.EURO);
            technicalBankAccount.setAccountType(AccountType.TECHNICAL);
            if (cardModel.getMonthlySalary() >= 500 && cardModel.getMonthlySalary() <= 1000) {
                technicalBankAccount.setInterest(0.1);
            } else {
                technicalBankAccount.setInterest(0.08);
            }
            technicalBankAccount.setUser_id(cardModel.getCreatedBy());
            technicalBankAccount.setIBAN(technicalBankAccount.getIBAN());
            bankAccountRepository.save(technicalBankAccount);

            CardModel savedCreditCard = cardRepository.findById(cardModel.getId()).orElseThrow(() -> new Exception("Can not find this Credit Card"));
            savedCreditCard.setBank_account_id(technicalBankAccount.getBankAccountId());
            savedCreditCard.setApproved(true);
            return cardRepository.save(savedCreditCard);
        } else {
            CardModel savedDebitCard = cardRepository.findById(cardModel.getId()).orElseThrow(() -> new Exception("Can not find this Credit Card"));
            savedDebitCard.setApproved(true);
            return cardRepository.save(savedDebitCard);
        }
    }

    public List<CardModel> getAllCards() {
        return (List<CardModel>) cardRepository.findAll();
    }

    public Optional<CardModel> getClientCard(Long id) {
        return cardRepository.findById(id);
    }

    public List<CardModel> getClientCards(String user_id) {
        return cardRepository.findAllByCreatedBy(user_id);
    }
}
