package com.backend.bankingapplication.controller;


import com.backend.bankingapplication.model.TransactionModel;
import com.backend.bankingapplication.service.TransactionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class TransactionController {
    private static final Logger logger = LoggerFactory.getLogger(TransactionController.class);

    @Autowired
    private TransactionService transactionService;

    @PostMapping("/new-transaction")
    @PreAuthorize("hasRole('client')")
    public TransactionModel createTransaction(@RequestBody TransactionModel transactionModel) throws Exception {
        logger.info("There is a new transaction");
        return transactionService.createTransaction(transactionModel);
    }

    @GetMapping("/transactions/{user_id}")
    @PreAuthorize("hasRole('client')")
    public List<TransactionModel> getClientTransactions(@PathVariable("user_id") String user_id) {
        return transactionService.getClientTransactions(user_id);
    }

    @GetMapping("/transactions")
    @PreAuthorize("hasRole('teller')")
    public List<TransactionModel> getAllTransactions() {
        return transactionService.getAllTransactions();
    }


}
