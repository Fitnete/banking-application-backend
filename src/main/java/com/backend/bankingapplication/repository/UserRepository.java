package com.backend.bankingapplication.repository;

import com.backend.bankingapplication.model.RoleModel;
import com.backend.bankingapplication.model.UserModel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;
import java.util.Optional;

public interface UserRepository extends CrudRepository<UserModel, String> {
    List<UserModel> findUserModelsByRoles(Optional<RoleModel> roles);
}
