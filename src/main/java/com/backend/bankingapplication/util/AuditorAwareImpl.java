package com.backend.bankingapplication.util;

import com.backend.bankingapplication.configuration.JwtRequestFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.AuditorAware;

import java.util.Optional;

public class AuditorAwareImpl implements AuditorAware<String> {

    @Autowired
    JwtRequestFilter jwtRequestFilter;


    @Override
    public Optional<String> getCurrentAuditor() {
        return Optional.of(jwtRequestFilter.getUsername());
    }
}
