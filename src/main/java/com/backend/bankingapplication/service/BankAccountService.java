package com.backend.bankingapplication.service;

import com.backend.bankingapplication.model.BankAccountModel;
import com.backend.bankingapplication.model.Currency;
import com.backend.bankingapplication.repository.BankAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;

@Service
public class BankAccountService {

    @Autowired
    private BankAccountRepository bankAccountRepository;

    public BankAccountModel createBankAccount(BankAccountModel bankAccountModel) {
        return bankAccountRepository.save(bankAccountModel);

    }

    public Currency[] getCurrencies() {
        return Currency.values();
    }

    public List<BankAccountModel> getUserBankAccounts(String user_id) {
        return bankAccountRepository.findByUser_id(user_id);
    }

    public List<BankAccountModel> getAllBankAccounts() {
        return (List<BankAccountModel>) bankAccountRepository.findAll();
    }

    public Optional<BankAccountModel> getClientBankAccount(Long bankAccountId) {
        return bankAccountRepository.findById(bankAccountId);
    }

    @Transactional
    public BankAccountModel approveClientBankAccount(Long bankAccountId, BankAccountModel updatedAccount) throws Exception {
        BankAccountModel bankAccount = bankAccountRepository.findById(bankAccountId).orElseThrow(() -> new Exception("Can not find this bank account"));
        bankAccount.setApproved(updatedAccount.isApproved());
        bankAccount.setIBAN(bankAccount.getIBAN());
        return bankAccountRepository.save(bankAccount);
    }
}
