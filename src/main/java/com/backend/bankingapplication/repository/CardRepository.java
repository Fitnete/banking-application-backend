package com.backend.bankingapplication.repository;

import com.backend.bankingapplication.model.CardModel;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface CardRepository extends CrudRepository<CardModel, Long> {
    List<CardModel> findAllByCreatedBy(String createdBy);

    @Query("select c from CardModel c where c.bank_account_id = ?1")
    CardModel findCardModelByBank_account_id(Long bank_account_id);
}
