package com.backend.bankingapplication.service;

import com.backend.bankingapplication.model.*;
import com.backend.bankingapplication.repository.BankAccountRepository;
import com.backend.bankingapplication.repository.CardRepository;
import com.backend.bankingapplication.repository.TransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;


@Service
public class TransactionService {

    @Autowired
    private TransactionRepository transactionRepository;

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private CardRepository cardRepository;

    @Transactional
    public TransactionModel createTransaction(TransactionModel transactionModel) throws Exception {

        BankAccountModel bankAccount = bankAccountRepository.findById(transactionModel.getBank_account_id())
                .orElseThrow(() -> new Exception("Can not find this bank account"));
        CardModel cardModel = cardRepository.findCardModelByBank_account_id(transactionModel.getBank_account_id());
        double currentBalance = bankAccount.getBalance();
        Integer amount = transactionModel.getAmount();
        Currency currency = bankAccount.getCurrency();
        AccountType accountType = bankAccount.getAccountType();
        Double cardLimit = cardModel.getCardLimit();
        double interest = bankAccount.getInterest();

        if (Objects.equals(accountType.toString(), "CURRENT")) {
            if (Objects.equals(transactionModel.getTransactionType().toString(), "CREDIT")) {
                transactionModel.setCurrency(currency);
                double addedBalance = currentBalance + amount;
                bankAccount.setBalance(addedBalance);
                bankAccountRepository.save(bankAccount);
                return transactionRepository.save(transactionModel);
            } else {
                if (amount <= currentBalance) {
                    transactionModel.setCurrency(currency);
                    double subtractedBalance = currentBalance - amount;
                    bankAccount.setBalance(subtractedBalance);
                    bankAccountRepository.save(bankAccount);
                    return transactionRepository.save(transactionModel);
                } else {
                    throw new Exception("You do not have enough balance to do this transaction!");
                }
            }
        } else {
            if (Objects.equals(transactionModel.getTransactionType().toString(), "DEBIT")) {
                if (amount <= cardLimit) {
                    transactionModel.setCurrency(currency);
                    double interestAmount = amount * interest;
                    double subtractedBalance = currentBalance - (amount + interestAmount);
                    bankAccount.setBalance(subtractedBalance);
                    double debitedAmount = amount + interestAmount;
                    double newCardLimit = cardLimit - debitedAmount;
                    cardModel.setCardLimit(newCardLimit);
                    bankAccountRepository.save(bankAccount);
                    return transactionRepository.save(transactionModel);
                } else {
                    throw new Exception("You do not have enough card limit to do this transaction!");
                }
            } else {
                throw new Exception("Invalid transaction! You can not credit your credit card!");
            }
        }
    }

    public List<TransactionModel> getClientTransactions(String user_id) {
        return transactionRepository.findByCreatedBy(user_id);
    }

    public List<TransactionModel> getAllTransactions() {
        return (List<TransactionModel>) transactionRepository.findAll();
    }
}
