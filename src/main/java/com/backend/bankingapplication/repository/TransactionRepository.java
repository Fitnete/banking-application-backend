package com.backend.bankingapplication.repository;

import com.backend.bankingapplication.model.TransactionModel;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface TransactionRepository extends CrudRepository<TransactionModel, Long> {

    List<TransactionModel> findByCreatedBy(String user_id);

}
