package com.backend.bankingapplication.controller;

import com.backend.bankingapplication.model.BankAccountModel;
import com.backend.bankingapplication.model.Currency;
import com.backend.bankingapplication.service.BankAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController()
public class BankAccountController {
    private static final Logger logger = LoggerFactory.getLogger(BankAccountController.class);

    @Autowired
    private BankAccountService bankAccountService;

    @PostMapping("/new-account")

    public BankAccountModel createBankAccount(@RequestBody BankAccountModel bankAccountModel) {
        logger.info("A new bank account was created");
        return bankAccountService.createBankAccount(bankAccountModel);
    }

    @GetMapping("/currencies")
    public Currency[] getCurrencies() {
        return bankAccountService.getCurrencies();
    }

    @GetMapping("/accounts/{user_id}")
    @PreAuthorize("hasRole('client')")
    public List<BankAccountModel> getClientBankAccounts(@PathVariable("user_id") String user_id) {
        return bankAccountService.getUserBankAccounts(user_id);
    }

    @GetMapping("/accounts")
    @PreAuthorize("hasRole('teller')")
    public List<BankAccountModel> getAllAccounts() {
        return bankAccountService.getAllBankAccounts();
    }

    @GetMapping("/account/{bankAccountId}")
    @PreAuthorize("hasRole('teller')")
    public Optional<BankAccountModel> getClientBankAccount(@PathVariable("bankAccountId") Long bankAccountId) {
        return bankAccountService.getClientBankAccount(bankAccountId);
    }

    @PutMapping("/updateAccount/{bankAccountId}")
    @PreAuthorize("hasRole('teller')")
    public BankAccountModel approveClientBankAccount(@PathVariable("bankAccountId") Long bankAccountId,
                                                     @RequestBody BankAccountModel bankAccountModel) throws Exception {
        return bankAccountService.approveClientBankAccount(bankAccountId, bankAccountModel);
    }
}
