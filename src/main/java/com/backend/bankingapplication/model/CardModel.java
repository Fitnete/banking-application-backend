package com.backend.bankingapplication.model;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "card")
public class CardModel extends Auditable {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "card_id", nullable = false)
    private Long id;
    private CardType cardType;
    private Integer monthlySalary;
    private Double cardLimit;
    private boolean approved = false;
    private Long bank_account_id;

}
