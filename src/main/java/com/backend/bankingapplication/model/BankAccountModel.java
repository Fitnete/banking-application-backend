package com.backend.bankingapplication.model;


import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;
import java.util.Random;


@Entity
@Getter
@Setter
@Table(name = "bank_account")
@AllArgsConstructor
@NoArgsConstructor
public class BankAccountModel extends Auditable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long bankAccountId;

    private String IBAN = "AL ";
    private Currency currency;
    private double balance = 0.00;
    private AccountType accountType;
    private double interest = 0;
    private boolean approved = false;
    private String user_id;

    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "bank_account_id", referencedColumnName = "bankAccountId")
    private CardModel cardModel;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(name = "bank_account_id", referencedColumnName = "bankAccountId")
    private List<TransactionModel> transactions;

    public void setIBAN(String IBAN) {
        Random rand = new Random();
        for (int i = 0; i < 28; i++) {
            int n = rand.nextInt(10);
            IBAN += Integer.toString(n);
        }

        this.IBAN = IBAN;
    }
}
