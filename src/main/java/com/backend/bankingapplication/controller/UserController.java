package com.backend.bankingapplication.controller;

import com.backend.bankingapplication.model.RoleModel;
import com.backend.bankingapplication.model.UserModel;
import com.backend.bankingapplication.service.UserService;
import jakarta.annotation.PostConstruct;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class UserController {
    private static final Logger logger = LoggerFactory.getLogger(UserController.class);

    @Autowired
    private UserService userService;

    @PostConstruct
    public void initRoleAndUser() {
        userService.initRoleAndUser();
    }

    @GetMapping("/tellers")
    @PreAuthorize("hasRole('admin')")
    public List<UserModel> getTellers() {
        return userService.getTellers();
    }

    @GetMapping("/getTeller/{userName}")
    @PreAuthorize("hasRole('admin')")
    public UserModel getTeller(@PathVariable String userName) throws Exception {
        return userService.getUser(userName);
    }

    @GetMapping("/getClient/{userName}")
    @PreAuthorize("hasRole('teller')")
    public UserModel getClient(@PathVariable String userName) throws Exception {
        return userService.getUser(userName);
    }


    @GetMapping("/clients")
    @PreAuthorize("hasRole('teller')")
    public List<UserModel> getClients() {
        return userService.getClients();
    }

    @PostMapping("/createTeller")
    @PreAuthorize("hasRole('admin')")
    public UserModel createTeller(@RequestBody UserModel teller) {
        logger.info("A new teller was created by admin");
        return userService.createTeller(teller);
    }


    @PostMapping("/createClient")
    @PreAuthorize("hasRole('teller')")
    public UserModel createClient(@RequestBody UserModel client) {
        logger.info("A new client was created by teller");
        return userService.createClient(client);
    }

    @PreAuthorize("hasRole('admin')")
    @DeleteMapping({"/deleteTeller/{userName}"})
    public void deleteTeller(@PathVariable String userName) {
        logger.info("Teller was deleted by admin");
        userService.deleteUser(userName);

    }

    @PreAuthorize("hasRole('teller')")
    @DeleteMapping({"/deleteClient/{userName}"})
    public void deleteClient(@PathVariable String userName) {
        logger.info("Client was deleted by teller");
        userService.deleteUser(userName);

    }

    @GetMapping("/roles")
    @PreAuthorize("hasRole('teller')")
    public List<RoleModel> getRoles() {
        return userService.getRoles();
    }


}
