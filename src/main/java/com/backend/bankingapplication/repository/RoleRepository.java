package com.backend.bankingapplication.repository;

import com.backend.bankingapplication.model.RoleModel;
import org.springframework.data.repository.CrudRepository;

public interface RoleRepository extends CrudRepository<RoleModel, String> {

}
